# Milankas training project

### Deployment

#### 1. Build executable jar file 

For this task you can use the next command:
```bash
./mvnw clean package -DskipTests
```

#### 2. Build Docker Image

```bash
docker-compose build
```

#### 3. Run Docker containers
```bash
docker-compose up
```

### 🔌Available Endpoints

In this application you can test the following endpoints:

`GET /v1/products/` Obtains all the products

`GET /v1/products/{productId}` Obtains the product for an specific productId

`POST /v1/products/` Inserts a product into the product collection

`PATCH /v1/products/{productId}` Updates partially an specific product

`DELETE /v1/products/{productId}` Deletes the product whit that specific Id
