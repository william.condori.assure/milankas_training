package com.milankas.training.web.controller;

import com.milankas.training.domain.dto.ProductRequestDTO;
import com.milankas.training.domain.dto.ProductResponseDTO;
import com.milankas.training.domain.dto.ProductUpdateDTO;
import com.milankas.training.domain.service.ProductService;
import com.milankas.training.utils.validation.ValidUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/products")
@Validated
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public ResponseEntity<List<ProductResponseDTO>> getAll(@RequestParam(value = "companyId", required = false) String companyId) {
        if (companyId != null) {
            return new ResponseEntity<>(productService.getAllByCompanyId(companyId), HttpStatus.OK);
        }
        return new ResponseEntity<>(productService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductResponseDTO> getProduct(
            @PathVariable("productId") @ValidUUID(message = "The id has  invalid UUID") String productId
    ) {
        return productService.getProduct(productId)
                .map(productRequestDTO -> new ResponseEntity<>(productRequestDTO, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/")
    public ResponseEntity<ProductResponseDTO> create(@Valid @RequestBody ProductRequestDTO productRequestDTO) {
        return new ResponseEntity(productService.create(productRequestDTO), HttpStatus.CREATED);
    }

    @PatchMapping("/{productId}")
    public ResponseEntity<ProductRequestDTO> patchProduct(
            @PathVariable("productId") @ValidUUID(message = "The id has  invalid UUID") String productId,
            @Valid @RequestBody ProductUpdateDTO productUpdateDTO
    ) {
        return productService.patch(productUpdateDTO, productId)
                .map(productUpdated -> new ResponseEntity<>(productUpdated, HttpStatus.CREATED))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<ProductRequestDTO> delete(
            @PathVariable("productId") @ValidUUID(message = "The id has  invalid UUID") String productId
    ) {
        if (productService.delete(productId)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/healthcheck")
    public ResponseEntity healthcheck(){
        HashMap<String, String> map = new HashMap<>();
        map.put("status", "Up");
        return ResponseEntity.ok(map);
    }
}
