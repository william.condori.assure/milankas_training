package com.milankas.training.clients;

import com.milankas.training.domain.dto.CompanyResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "microservice-company")
@RequestMapping("/v1/companies")
public interface CompanyClientRest {

    @GetMapping("/{companyId}")
    ResponseEntity<CompanyResponseDTO> getCompany(@PathVariable("companyId") String companyId);

}
