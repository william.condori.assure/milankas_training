package com.milankas.training.persistence.crud;

import com.milankas.training.persistence.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ProductCrudRepository extends JpaRepository<Product, UUID> {

    List<Product> findAllByCompanyId(UUID companyId);

}
