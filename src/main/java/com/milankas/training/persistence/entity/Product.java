package com.milankas.training.persistence.entity;

import com.milankas.training.persistence.converters.StringListConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    private String description;
    @Column(name = "company_id")
    private UUID companyId;
    private Boolean blocked;
    @Convert(converter = StringListConverter.class)
    private List<String> categories;

}
