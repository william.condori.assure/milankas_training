package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.dto.ProductRequestDTO;
import com.milankas.training.domain.dto.ProductResponseDTO;
import com.milankas.training.domain.dto.ProductUpdateDTO;
import com.milankas.training.persistence.entity.Product;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {HelperMapper.class}, collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE)
public interface ProductMapper {

    @Mappings({
            @Mapping(target = "id", qualifiedByName = "UUIDtoString"),
            @Mapping(target = "companyId", qualifiedByName = "UUIDtoString")
    })
    ProductRequestDTO toProductRequestDTO(Product product);

    ProductResponseDTO toProductResponseDTO(Product product);

    List<ProductResponseDTO> toListProductResponseDTO(List<Product> products);

    @Mappings({
            @Mapping(target = "companyId", qualifiedByName = "stringToUUID"),
            @Mapping(target = "id", ignore = true)
    })
    @InheritInverseConfiguration
    Product toProduct(ProductRequestDTO productRequestDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "companyId", qualifiedByName = "stringToUUID")
    Product toProductPatch(ProductUpdateDTO productUpdateDTO, @MappingTarget Product product);
}
