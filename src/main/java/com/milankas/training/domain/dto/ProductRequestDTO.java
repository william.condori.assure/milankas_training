package com.milankas.training.domain.dto;

import com.milankas.training.utils.validation.ValidUUID;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class ProductRequestDTO {

    private String id;

    @Size(min = 3, max = 50)
    @NotNull(message = "name is required")
    private String name;

    @Size(min = 3, message = "size must be grater than 3 characters")
    @NotNull
    private String description;

    @NotNull
    @ValidUUID(message = "The UUID format is wrong")
    private String companyId;

    @NotNull(message = "this attribute is required")
    @Pattern(regexp = "^true$|^false$", message = "allowed input: true or false")
    private String blocked;

    @NotNull
    private List<@NotNull String> categories;

    public boolean getBlocked() {
        return Boolean.parseBoolean(blocked);
    }

}
