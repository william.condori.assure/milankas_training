package com.milankas.training.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyResponseDTO {

    private String id;
    private String name;
    private AddressResponseDTO address;

}
