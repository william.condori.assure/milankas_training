package com.milankas.training.domain.dto.log;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class LogDTO {

    private String level;
    private Date createdAt;
    private String serviceName;
    private String message;

}
