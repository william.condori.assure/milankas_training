package com.milankas.training.domain.dto.log;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LogMessageDTO {
    private Integer httpStatus;
    private String httpMethod;
    private String path;
    private String clientIp;
    private String response;
    private Map<String, String> headers;
    private Map<String, String> parameters;
}
