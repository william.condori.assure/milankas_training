package com.milankas.training.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressResponseDTO {

    private String id;
    private String addressLine1;
    private String addressLine2;
    private String state;
    private String city;
    private String zipCode;
    private String countryCode;

}
