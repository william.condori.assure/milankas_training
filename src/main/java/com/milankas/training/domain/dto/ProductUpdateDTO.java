package com.milankas.training.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.milankas.training.utils.validation.ValidUUID;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class ProductUpdateDTO {

    @JsonIgnore
    private UUID id;

    @Size(min = 3, max = 50)
    private String name;

    @Size(min = 3, message = "size must be grater than 3 characters")
    private String description;

    @ValidUUID(message = "The UUID format is wrong")
    private String companyId;

    @Pattern(regexp = "^true$|^false$", message = "allowed input: true or false")
    private String blocked;

    private List<String> categories;

    public Boolean getBlocked() {
        return blocked == null ? null : Boolean.parseBoolean(blocked);
    }
}
