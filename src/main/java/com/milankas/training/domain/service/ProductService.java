package com.milankas.training.domain.service;

import com.milankas.training.clients.CompanyClientRest;
import com.milankas.training.domain.dto.ProductRequestDTO;
import com.milankas.training.domain.dto.ProductResponseDTO;
import com.milankas.training.domain.dto.ProductUpdateDTO;
import com.milankas.training.persistence.crud.ProductCrudRepository;
import com.milankas.training.persistence.entity.Product;
import com.milankas.training.persistence.mapper.ProductMapper;
import com.milankas.training.web.controller.exception.ApiRequestException;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductCrudRepository productRepository;

    @Autowired
    private ProductMapper mapper;

    @Autowired
    private CompanyClientRest companyClientRest;

    public List<ProductResponseDTO> getAll() {
        return mapper.toListProductResponseDTO(productRepository.findAll());
    }

    public List<ProductResponseDTO> getAllByCompanyId(String companyId) {
        return mapper.toListProductResponseDTO(productRepository.findAllByCompanyId(UUID.fromString(companyId)));
    }

    public Optional<ProductResponseDTO> getProduct(String productId) {
        return productRepository.findById(UUID.fromString(productId))
                .map(mapper::toProductResponseDTO);
    }

    public ProductResponseDTO create(ProductRequestDTO productRequestDTO) {
        try {
            companyClientRest.getCompany(productRequestDTO.getCompanyId()).getStatusCodeValue();
            return mapper.toProductResponseDTO(productRepository.save(mapper.toProduct(productRequestDTO)));
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                throw new ApiRequestException("companyId doesn't exist");
            }
            throw new ApiRequestException("microservice-category is not available");
        }
    }

    public Optional<ProductRequestDTO> patch(ProductUpdateDTO productUpdateDTO, String productId) {
        try {
            if (productUpdateDTO.getCompanyId() != null) {
                companyClientRest.getCompany(productUpdateDTO.getCompanyId());
            }

            return productRepository.findById(UUID.fromString(productId))
                    .map(product -> {
                        Product productPatch = mapper.toProductPatch(productUpdateDTO, product);
                        return mapper.toProductRequestDTO(productRepository.save(productPatch));
                    });
        } catch (FeignException e) {
            if (e.status() == HttpStatus.NOT_FOUND.value()) {
                throw new ApiRequestException("companyId doesn't exist");
            }
            throw new ApiRequestException("microservice-company is not available");
        }
    }

    public boolean delete(String productId) {
        return getProduct(productId).map(product -> {
            productRepository.deleteById(UUID.fromString(productId));
            return true;
        }).orElse(false);
    }
}
