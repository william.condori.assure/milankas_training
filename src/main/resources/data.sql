DELETE
FROM product;
INSERT INTO product(id, name, description, company_id, blocked, categories)
VALUES ('952c077a-cd32-4d62-8c37-c23250cd4308', 'Apple',
        'The apple is an edible fruit. It is shaped like a globe, a little sunken at its ends.',
        'd8827313-8e7a-473a-8b27-40d8c71af632', false, 'Fruits and vegetables;Household food'),
       ('6fba5c17-73de-4bd5-bc2d-62532f95e114', 'Banana',
        'The banana is an edible fruit. It is shaped like a globe, a little sunken at its ends.',
        '33f953f4-c913-428e-8329-6c8e29463467', false, 'Fruits and vegetables;Household food');