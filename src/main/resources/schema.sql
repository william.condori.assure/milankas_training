-- Creation of product table
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS product
(
    id          uuid NOT NULL DEFAULT uuid_generate_v4(),
    name        character varying(50) COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    company_id  uuid NOT NULL,
    blocked     boolean NOT NULL,
    categories  text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT  product_pkey PRIMARY KEY (id)
);