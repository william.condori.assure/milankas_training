-- Creation of product table
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE product
(
    id          uuid DEFAULT uuid_generate_v4(),
    name        VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    company_id   uuid         NOT NULL,
    blocked     BOOLEAN      NOT NULL,
    categories  TEXT         NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO product(name, description, company_id, blocked, categories)
VALUES ('Apple', 'The apple is an edible fruit. It is shaped like a globe, a little sunken at its ends.',
        'f1b23046-fd1c-4397-9723-25b394d0106f', false, 'Fruits and vegetables;Household food')