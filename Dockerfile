FROM openjdk:8-jdk-alpine
COPY target/product-api-0.1.0.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]